��          �      l      �  6   �  5        N     b     h  "   }     �     �  2   �     �     �     	          /     ?  �   ]  �   �     �     �     �  Q  �  6     D   G     �     �     �      �     �     �  5   �     3     C     T  /   b     �     �  �   �  �   ~     5	  	   <	     F	               	                                                         
                          <a href="https://ggnome.com">Garden Gnome Software</a> A custom block for displaying a Garden Gnome Package. Default Player Size GGPKG Garden Gnome Package Garden Gnome Package Documentation General Settings Height Import Pano2VR & Object2VR Content into Wordpress. Player Version Select Package Settings Settings could not be saved. Settings saved. Start player as preview image The PHP Zip extension is not installed on your server. Without it the Garden Gnome Package plugin will not work. Please contact your server administrator. The libxml extension is not installed on your server. Without it the Garden Gnome Package plugin will not work. Please contact your server administrator. Width from package https://ggnome.com/ggpkg PO-Revision-Date: 2019-06-27 11:09+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.2.3
Language: de
Project-Id-Version: Plugins - Garden Gnome Package - Development (trunk)
POT-Creation-Date: 
Last-Translator: 
Language-Team: 
 <a href="https://ggnome.com">Garden Gnome Software</a> Ein benutzerdefinierter Block zur Anzeige eines Garten Gnome Pakets. Standard Anzeigegröße GGPKG Garden-Gnome-Paket Garten-Gnome-Paket Dokumentation Allgemeine Einstellungen Höhe Import von Pano2VR & Objekt2VR-Inhalten in WordPress. Softwareversion Paket auswählen Einstellungen Einstellungen konnten nicht gespeichert werden. Einstellungen gespeichert. Starte als Vorschau-Bild Die PHP-Zip-Erweiterung ist nicht auf Ihrem Server installiert. Ohne sie wird das Garden Gnome Package-Plugin nicht funktionieren. Bitte wenden Sie sich an Ihren Server-Administrator. Die libxml-Erweiterung ist nicht auf Ihrem Server installiert. Ohne sie wird das Garden Gnome Package-Plugin nicht funktionieren. Bitte wenden Sie sich an Ihren Server-Administrator. Breite vom Paket https://ggnome.com/ggpkg 
import '../style/main.scss';
import '@fancyapps/fancybox/dist/jquery.fancybox.css';
import $ from 'jquery';
import Slider from './module/slider';
import menu from './module/menu';
import mobileMenu from './module/mobile-menu';
import validation from './module/validation-form';
// import starHover from './module/star-hover';
import testimonialsRating from './module/testimonials-rewiev-rating';
import mailingSubscribe from './module/mailing';
import appointmentForm from './module/appointment-form';
import gallery from './module/gallery';
import localizedCountries from './module/localized-countries';
import accordion from './module/accordion';
// import comet from './module/comet';
import patientsCards from './module/patients-cards';
import tablePressAutowrap from './module/table-press-autowrap';
import dateInput from './module/date-input';
import wechatScroll from './module/wechatScroll';

$(document).ready(() => {
  new Slider('.js-main');
  new Slider('.js-testimonials');
  new Slider('.js-treatments', {arrows: false}, true);
  new Slider('.js-why-us', {arrows: false}, true);
  new Slider('.js-photogallery-slider', {autoplay: false});

  // new Slider('.js-blog-main');
  new Slider('.js-blog');
  new Slider('.js-blog-archive', {}, true);
  new Slider('.js-services', {}, true);
  new Slider('.js-healing', {}, 575);

  wechatScroll();
  dateInput();
  menu();
  mobileMenu();
  validation();
  // starHover();
  testimonialsRating();
  mailingSubscribe();
  appointmentForm();
  gallery();
  localizedCountries();
  accordion();
  // comet();
  patientsCards();
  tablePressAutowrap();
});

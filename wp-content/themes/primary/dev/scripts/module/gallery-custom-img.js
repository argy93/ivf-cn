import $ from 'jquery';
import '@fancyapps/fancybox/dist/jquery.fancybox';

export default () => {
  if ($('main').find('.js-gallery-custom-item')) {
    $('.js-gallery-custom-item').attr("data-fancybox", "gallery");
    $('.js-gallery-custom-item').fancybox();
  }

};

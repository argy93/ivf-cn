import $ from 'jquery';
import 'jquery-validation';

export default () => {
  $(document).ready(() => {
    $('.c-form .u-btn-main').click((event) => {
      event.preventDefault();

      const subscribeForm = $('.c-form');
      subscribeForm
        .siblings('.c-form__warning')
        .text('')
        .removeClass('is-active');

      if (subscribeForm.valid()) {
        const emailInput = $('.c-form input');
        const emailVal = emailInput.val();

        $.ajax({
          method: 'POST',
          url: '/wp-json/newsletter/v1/subscribe',
          dataType: 'json',
          data: {
            email: emailVal,
          },
          success: () => {
            subscribeForm
              .children('input')
              .removeClass('error');

            const formUrl = subscribeForm.attr('data-url');
            // eslint-disable-next-line no-restricted-globals
            $(location).attr('href', formUrl);
          },
          error: (status) => {
            if (status.responseJSON.message === 'Email address already exists') {
              subscribeForm
                .children('input')
                .addClass('error');

              subscribeForm
                .siblings('.c-form__warning')
                .text(status.responseJSON.message)
                .addClass('is-active');
            }
          },
        });
      }
    });
  });
};

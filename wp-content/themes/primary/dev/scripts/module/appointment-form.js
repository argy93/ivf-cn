import $ from 'jquery';
import 'jquery-validation';

export default () => {
  const $form = $('.js-appointment-form');

  $form.submit((event) => {
    event.preventDefault();

    if (!formValid($form)) {
      return;
    }

    const formData = getFormData($form);

    formDataRequest($form, formData);

    $form.addClass('is-sending');

    emptyFormHandler($form);
  });
};

const formValid = $form => $form.valid();

const getFormData = ($form) => {
  const formData = new FormData();

  formData.append('name', $form.find('input[name="name"]').val());
  formData.append('phone', $form.find('input[name="phone"]').val());
  formData.append('email', $form.find('input[name="email"]').val());
  formData.append('date', $form.find('input[name="date"]').val());
  formData.append('city', $form.find('input[name="city"]').val());
  formData.append('message', $form.find('textarea[name="message"]').val());
  formData.append('country', $form.find('select[name="country"]').val());

  return formData;
};

const formDataRequest = ($form, formData) => {
  $.ajax({
    url: '/wp-json/primary/v1/forms/rest-visit-doctor',
    data: formData,
    processData: false,
    contentType: false,
    type: 'POST',
    success() {
      const formUrl = $form.attr('data-url');

      // eslint-disable-next-line no-restricted-globals
      $(location).attr('href', formUrl);
    },
  });
};

const emptyFormHandler = ($form) => {
  $form.find('input').val('');
  $form.find('textarea').val('');
  $form.find('.is-changed').remove();
  $form.find('.c-contact-form__files').empty();
};

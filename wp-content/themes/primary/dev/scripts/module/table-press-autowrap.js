import $ from 'jquery';
// Module description
export default () => {
  // return;
  $('.js-scroll-table').each(init);

  function init() {
    $(this).css('minWidth', 800);
    $(this).wrap('<div style="overflow-x: auto;"></div>');
  }
};

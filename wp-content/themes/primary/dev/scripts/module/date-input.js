import $ from 'jquery';

export default () => {
  $('.js-input-mask-text').click((e) => {
    $(e.currentTarget).parent().addClass('d-none');
    $('.js-input-mask-date').removeClass('d-none');
  });
};

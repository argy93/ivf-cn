import $ from 'jquery';
import 'slick-carousel';

export default () => {
  const $main = $('.js-slick-example');

  if (!$main.length) return;

  $main.slick();
};

import $ from 'jquery';

export default () => {
  $('.c-post-faq__heading').click((event) => {
    accordion(event.currentTarget);

    setTimeout(() => {
      const eTop = $(event.currentTarget).offset().top;

      $('body,html').animate({
        scrollTop: eTop,
      }, 1000);
    }, 400);
  });
};

const accordion = (el) => {
  const accordionHead = $(el).siblings();
  const item = $('.c-post-faq__item');

  item
    .find('.c-post-faq__heading')
    .not(el)
    .removeClass('is-active')
    .next()
    .slideUp(300);

  if ($(el).hasClass('is-active')) {
    accordionHead
      .slideUp(300)
      .prev()
      .removeClass('is-active');
  } else {
    accordionHead
      .slideDown(300)
      .prev()
      .addClass('is-active');
  }
};

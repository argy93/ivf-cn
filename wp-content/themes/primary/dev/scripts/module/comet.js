import $ from 'jquery';
import 'jquery.cookie';

export default () => {
  const delay = 14000;
  const comet = $('.js-comet');

  if (!comet.length) {
    return;
  }


  // eslint-disable-next-line no-restricted-globals
  if (window.location.pathname === '/') {
    $.removeCookie('is-comet-show', {path: '/'});
  }

  if ($.cookie('is-comet-show') === 'null' || !$.cookie('is-comet-show')) {
    const catchJivo = setInterval(() => {
      if (window.jivo_api) {
        clearInterval(catchJivo);
        setTimeout(() => {
          dropComet(comet);
        }, delay);
      }
    }, 100);
  }
};

const dropComet = (_comet) => {
  const comet = _comet;
  const offsetLeft = getChatPosition();
  if (offsetLeft) {
    comet[0].style.transition = 'none';
    comet.css('left', `${offsetLeft}px`);
    setTimeout(() => {
      comet[0].style.transition = '';
      comet.addClass('is-animate');
    }, 10);
  }
  setTimeout(() => {
    // eslint-disable-next-line no-undef
    if (window.innerWidth > 767) jivo_api.open();
    $.cookie('is-comet-show', true, {path: '/'});
    comet.fadeOut(600);
  }, 1400);
};

const getChatPosition = () => {
  const mobile = $('.wrap_mW');
  const desctop = $('#jvlabelWrap');
  let target = false;

  if (mobile.length) {
    target = mobile;
  } else if (desctop.length) {
    target = desctop;
  }

  if (target) {
    return target.offset().left;
  }

  return 0;
};

import $ from 'jquery';

export default () => {
  $('.js-wechat').click((e) => {
    e.preventDefault();
    const id = $(e.currentTarget).attr('href');
    const topOffset = $(id).offset().top;
    const period = 1500;

    $('body,html').animate({scrollTop: topOffset}, period);

    setTimeout(() => {
      $('#is-qr').find('img').css('transform', 'scale(1.1)');

      setTimeout(() => {
        $('#is-qr').find('img').css('transform', 'scale(1)');
      }, period);
    }, period);
  });
};

import $ from 'jquery';
import 'slick-carousel';

export default () => {
  if ($(window).innerWidth() <= 576) {
    $('.js-why-us__slider').slick({
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1500,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      customPaging: (slider, i) => `<span class="c-dots-block__dot" data-thumb="${i}" ></span>`,
      appendDots: $('.l-why-us .c-dots-block'),
      touchMove: false,
      pauseOnFocus: false,
      pauseOnHover: false,
    });
  }
};

import $ from 'jquery';
import 'slick-carousel';

export default () => {
  $('.js-main__slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1000,
    fade: true,
    dots: true,
    customPaging: (slider, i) => `<span class="c-dots-block__dot" data-thumb="${i}" ></span>`,
    appendDots: $('.l-main .c-dots-block'),
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    touchMove: false,
    pauseOnFocus: false,
    pauseOnHover: false,
  });
};

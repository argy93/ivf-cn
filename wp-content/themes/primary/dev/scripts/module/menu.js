/* eslint-disable func-names */
import $ from 'jquery';

export default () => {
  if ($(window).innerWidth() < 992) {
    const header__item = $('.c-social__item');
    const header__close = $('.c-social__close');

    header__item.click((event) => {
      event.stopPropagation();

      const dropdown__items = $(event.currentTarget).find('.c-social__dropdown a');

      const dropdown = $(event.currentTarget).find('.c-social__dropdown');

      if (dropdown__items.length > 1 && $(window).innerWidth() >= 1024) {
        if (dropdown.hasClass('is-active')) {
          header__item.find('.c-social__dropdown').removeClass('is-active');
        } else {
          header__item.find('.c-social__dropdown').removeClass('is-active');
          dropdown.addClass('is-active');
        }
      } else if ($(window).innerWidth() <= 1024) {
        if (dropdown.hasClass('is-active')) {
          header__item.find('.c-social__dropdown').removeClass('is-active');
        } else {
          header__item.find('.c-social__dropdown').removeClass('is-active');
          dropdown.addClass('is-active');
        }
      }
    });

    $('.c-social__dropdown').click((event) => {
      event.stopPropagation();
    });

    header__close.click((event) => {
      event.stopPropagation();

      $(event.currentTarget).parent().removeClass('is-active');
    });

    $('body').click((event) => {
      if (!($(event.currentTarget).header__item)) {
        header__item.find('.c-social__dropdown').removeClass('is-active');
      }
    });
  }

  if ($(window).innerWidth() >= 1180) {
    $('.c-main-nav__item').hover(
      (event) => {
        $('.c-main-nav__item').removeClass('is-active');
        $(event.currentTarget).addClass('is-active');
      },
      (event) => {
        $(event.currentTarget).removeClass('is-active');
      },
    );

    $('.c-main-subnav li').hover((event) => {
      if ($(event.currentTarget).hasClass('c-item-2')) {
        $('.c-item-2').removeClass('is-active');
        $(event.currentTarget).addClass('is-active');
      }

      if ($(event.currentTarget).hasClass('c-item-3')) {
        $('.c-item-3').removeClass('is-active');
        $(event.currentTarget).addClass('is-active');
      }

      let maxHeight = 0;
      $('.c-main-subnav ul').height('auto');

      $('.c-main-nav__item.is-active .c-main-subnav ul:visible').each(function () {
        const curentHeight = $(this).height();
        if (curentHeight > maxHeight) {
          maxHeight = curentHeight;
        }
      });

      $('.c-main-nav__item.is-active .c-main-subnav ul:visible').height(maxHeight);
    });
  }
};

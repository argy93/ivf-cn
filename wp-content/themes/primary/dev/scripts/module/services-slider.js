import $ from 'jquery';
import 'slick-carousel';

export default () => {
  if ($(window).innerWidth() <= 991) {
    $('.js-services-slider').slick({
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1000,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '.l-services .c-arrows__prev',
      nextArrow: '.l-services .c-arrows__next',
      touchMove: false,
      pauseOnFocus: false,
      pauseOnHover: false,
      responsive: [
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            arrow: false,
            dots: true,
            customPaging: (slider, i) => `<span class="c-dots-block__dot" data-thumb="${i}" ></span>`,
            appendDots: $('.l-services .c-dots-block'),
          },
        },
      ],
    });
  }
};

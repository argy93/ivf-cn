import $ from 'jquery';
import jQueryBridget from 'jquery-bridget';
import Masonry from 'masonry-layout';
import imagesLoaded from 'imagesloaded';
import '@fancyapps/fancybox/dist/jquery.fancybox';

jQueryBridget('masonry', Masonry, $);
imagesLoaded.makeJQueryPlugin($);

// Галерея с плиткой
export default () => {
  const masonry_config = {
    columnWidth: 285,
    itemSelector: '.js-gallery-item',
  };

  if (window.innerWidth < 1200) {
    masonry_config.columnWidth = '.js-default';
  }

  const masonry = $('.js-gallery');

  if (!masonry.length) return;

  masonry.masonry(masonry_config);
  masonry.imagesLoaded().progress(() => {
    // console.log('draw', masonry_config.columnWidth);
    masonry.masonry('layout');
  });

  $(window).scroll(() => {
    if (window.scrollY + window.innerHeight > masonry.offset().top + masonry.height() - 300) {
      masonry.find('.js-gallery-item:hidden:lt(8)').css('display', 'block');
      masonry.masonry('layout');
    }
  });

  $('[data-fancybox="gallery"]').fancybox();
};

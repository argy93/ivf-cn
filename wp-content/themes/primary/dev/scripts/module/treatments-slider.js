import $ from 'jquery';
import 'slick-carousel';

export default () => {
  const $main = $('.js-treatments-slider');
  if ($(window).innerWidth() <= 991) {
    $main.slick({
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1500,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '.l-treat .c-arrows__prev',
      nextArrow: '.l-treat .c-arrows__next',
      touchMove: false,
      pauseOnFocus: false,
      pauseOnHover: false,
      responsive: [
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            arrows: false,
            dots: true,
            customPaging: (slider, i) => `<span class="c-dots-block__dot" data-thumb="${i}" ></span>`,
            appendDots: $('.l-treat .c-dots-block'),
          },
        },
      ],
    });
  }

  $main.find
};

// Module description
import $ from 'jquery';
import 'slick-carousel';

export default (rootClass = '.js-slider', updateConfig = {}, onlyMobile = false) => {
  const $root = $(rootClass);

  if (!$root.length) return;

  const config = {
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1500,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: `${rootClass}-wrap ${rootClass}__prev`,
    nextArrow: `${rootClass}-wrap ${rootClass}__next`,
    touchMove: false,
    pauseOnFocus: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          arrow: false,
          dots: true,
          customPaging: (slider, i) => `<span class="c-dots-block__dot" data-thumb="${i}" ></span>`,
          appendDots: $(`${rootClass}-wrap ${rootClass}__dots`),
        },
      },
    ],
  };
  const breakpoint = typeof onlyMobile === 'number' ? onlyMobile : 991;
  let isInit = false;
  $.extend(config, updateConfig);
  init();
  $(window).resize(init);

  function init() {
    if (onlyMobile && window.innerWidth > breakpoint) {
      stop();
    } else if (!onlyMobile) {
      run();
    } else if (onlyMobile && window.innerWidth <= breakpoint) {
      run();
    }

    function run() {
      if (!isInit) $root.slick(config);
      isInit = true;
    }

    function stop() {
      if (isInit) $root.slick('unslick');
      isInit = false;
    }
  }
};

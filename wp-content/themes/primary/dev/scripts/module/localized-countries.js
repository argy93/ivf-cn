import $ from 'jquery';
import 'localized-countries';

export default () => {
  const countrySelect = $('select[name="country"]');

  if (countrySelect.length) {
    const lang = countrySelect.attr('data-language');
    // eslint-disable-next-line import/no-dynamic-require,global-require
    const countries = require('localized-countries')(require(`localized-countries/data/${lang}`));

    const countriesArr = countries.array();

    $(countriesArr).each((index, item) => {
      $('select[name="country"]').append(`<option value="${item.code}">${item.label}</option>`);
    });
  }
};

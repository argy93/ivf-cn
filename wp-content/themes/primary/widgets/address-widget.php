<?php


class address_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
			'address_widget',
			// Widget name will appear in UI
			__( 'Address List', 'primary' ),
			// Widget description
			array( 'description' => __( 'Dropdown address list, on your site', 'primary' ), )
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		if ( isset( $instance['address'] ) ) {
			$address = json_decode($instance['address']);
		} else {
			$address = [''];
		}

		$context          = Timber::context();
		$context['address'] = [];
		$context['mail'] = get_option('admin_email');;
		$context['widget_id'] = $args['id'];;

		foreach ($address as $phone) {
			$context['address'][] = $phone;
		}

		Timber::render( array( 'templates/widgets/address-widget-front.twig' ), $context );
	}

	// Widget Backend
	public function form( $instance ) {

		if ( isset( $instance['address'] ) ) {
			$address = json_decode($instance['address']);
		} else {
			$address = [''];
		}

		$context          = Timber::context();

		$context['address'] = [
			'id'    => $this->get_field_id( 'address' ),
			'name'  => $this->get_field_name( 'address' ),
			'values' => $address,
			'e' => __( 'Address:', 'primary' )
		];

		Timber::render( array( 'templates/widgets/address-widget-back.twig' ), $context );
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance          = array();

		if (!empty($new_instance['address'])) {
			$instance['address'] = json_encode($new_instance['address']);
		} else {
			$instance['address'] = json_encode(['']);
		}

		return $instance;
	}
}

register_widget( 'address_widget' );

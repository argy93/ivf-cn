<?php


class socials_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
    // Base ID of your widget
      'socials_widget',
      // Widget name will appear in UI
      __( 'Socials', 'primary' ),
      // Widget description
      array( 'description' => __( '', 'primary' ), )
    );
  }

  // Creating widget front-end

  public function widget( $args, $instance ) {
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) ) {
      echo $args['before_title'] . $title . $args['after_title'];
    }

    $context = Timber::context();
    // $context['facebook'] = isset( $instance['facebook'] ) ? $instance['facebook'] : '';
    $context['weibo'] = isset( $instance['weibo'] ) ? $instance['weibo'] : '';
    $context['wechat'] = isset( $instance['wechat'] ) ? $instance['wechat'] : '';
    $context['skype'] = isset( $instance['skype'] ) ? $instance['skype'] : '';

    Timber::render( array( 'templates/widgets/socials-widget-front.twig' ), $context );
    // This is where you run the code and display the output
    echo $args['after_widget'];
  }

  // Widget Backend
  public function form( $instance ) {

    $context          = Timber::context();

    // $context['facebook'] = [
    //   'id'    => $this->get_field_id( 'facebook' ),
    //   'name'  => $this->get_field_name( 'facebook' ),
    //   'value' => isset($instance['facebook']) ? $instance['facebook'] : '',
    //   'e' => __( 'Facebook:', 'primary' )
    // ];
    $context['weibo'] = [
      'id'    => $this->get_field_id( 'weibo' ),
      'name'  => $this->get_field_name( 'weibo' ),
      'value' => isset($instance['weibo']) ? $instance['weibo'] : '',
      'e' => __( 'weibo:', 'primary' )
    ];
    $context['wechat'] = [
      'id'    => $this->get_field_id( 'wechat' ),
      'name'  => $this->get_field_name( 'wechat' ),
      'value' => isset($instance['wechat']) ? $instance['wechat'] : '',
      'e' => __( 'Youtube:', 'primary' )
    ];
    $context['skype'] = [
      'id'    => $this->get_field_id( 'skype' ),
      'name'  => $this->get_field_name( 'skype' ),
      'value' => isset($instance['skype']) ? $instance['skype'] : '',
      'e' => __( 'Skype:', 'primary' )
    ];

    Timber::render( array( 'templates/widgets/socials-widget-back.twig' ), $context );
  }

  // Updating widget replacing old instances with new
//  public function update( $new_instance, $old_instance ) {
//    return $new_instance;
//  }
}

register_widget( 'socials_widget' );

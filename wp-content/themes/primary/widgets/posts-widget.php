<?php


class posts_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
    // Base ID of your widget
      'posts_widget',
      // Widget name will appear in UI
      __( 'Posts List', 'primary' ),
      // Widget description
      array( 'description' => __( 'Dropdown posts list, on your site', 'primary' ), )
    );
  }

  // Creating widget front-end

  public function widget( $args, $instance ) {
    if ( isset( $instance['posts'] ) ) {
      $posts = json_decode($instance['posts']);
    } else {
      $posts = [''];
    }

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) ) {
      echo $args['before_title'] . $title . $args['after_title'];
    }

    $context          = Timber::context();
    $context['posts'] = [];

    foreach ($posts as $post) {
      $context['posts'][] = $post;
    }

    Timber::render( array( 'templates/widgets/posts-widget-front.twig' ), $context );
    // This is where you run the code and display the output
    echo $args['after_widget'];
  }

  // Widget Backend
  public function form( $instance ) {

    if ( isset( $instance['posts'] ) ) {
      $posts = json_decode($instance['posts']);
    } else {
      $posts = [''];
    }

    $context          = Timber::context();

    $context['posts'] = [
      'id'    => $this->get_field_id( 'posts' ),
      'name'  => $this->get_field_name( 'posts' ),
      'values' => $posts,
      'e' => __( 'Posts:', 'primary' )
    ];

    Timber::render( array( 'templates/widgets/posts-widget-back.twig' ), $context );
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance          = array();

    if (!empty($new_instance['posts'])) {
      $instance['posts'] = json_encode($new_instance['posts']);
    } else {
      $instance['posts'] = json_encode(['']);
    }

    return $instance;
  }
}

register_widget( 'posts_widget' );

<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */
include 'vendor/autoload.php';
use Wa72\HtmlPageDom\HtmlPageCrawler;

if ( ! class_exists( 'Timber' ) ) {
  add_action( 'admin_notices', function () {
    echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
  } );

  return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {
  /** Add timber support. */
  public function __construct() {
    include( get_template_directory() . '/includes/rest.php' );
    include( get_template_directory() . '/includes/language.php' );

    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_static' ) );
    add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
    add_action( 'widgets_init', array( $this, 'register_widgets' ) );
    add_filter( 'timber/context', array( $this, 'add_to_context' ) );
    add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
    add_action( 'init', array( $this, 'register_post_types' ) );
    add_action( 'init', array( $this, 'register_taxonomies' ) );
    add_filter( 'get_search_form', array( $this, 'custom_search_form' ), 100 );
    parent::__construct();
  }

  public function custom_search_form( $form ) {
    $translated_search = apply_filters( 'wpml_translate_single_string', 'Search', 'primary', 'Search form - placeholder' );

    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
				    <input autocomplete="off" placeholder="' . $translated_search . '" type="text" value="' . get_search_query() . '" name="s" id="s" />
				    <button type="submit" id="searchsubmit"><svg><use xlink:href="#icon-search"></use></svg></button>
			    </form>';

    return $form;
  }


  public function register_widgets() {
    include_once get_template_directory() . '/widgets/phones-widget.php';
    include_once get_template_directory() . '/widgets/address-widget.php';
    include_once get_template_directory() . '/widgets/cabinet-widget.php';
    include_once get_template_directory() . '/widgets/socials-widget.php';
    include_once get_template_directory() . '/widgets/posts-widget.php';

    register_sidebar( array(
      'name'          => 'Header top',
      'id'            => 'header_top',
      'before_widget' => '',
      'after_widget'  => '',
    ) );

    register_sidebar( array(
      'name'          => 'Footer top',
      'id'            => 'footer_top',
      'before_widget' => '',
      'after_widget'  => '',
    ) );

    register_sidebar( array(
      'name'          => 'Fixed',
      'id'            => 'fixed',
      'before_widget' => '',
      'after_widget'  => '',
    ) );
  }

  /** This is where you can register custom post types. */
  public function register_post_types() {

  }

  /** This is where you can register custom taxonomies. */
  public function register_taxonomies() {

  }

  public function enqueue_static() {
    $version = StarterSite::getVersion();

    wp_enqueue_style( 'style-main', get_template_directory_uri() . '/static/main-' . $version . '.css', array(), '1.0.0' );

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'script-main', get_template_directory_uri() . '/static/main-' . $version . '.js', array( 'jquery' ), '1.0.0', true );
  }

  public static function getVersion() {
    $version = @file_get_contents( get_template_directory() . '/version.md5' );

    if ( ! $version ) {
      return 1;
    }

    return $version;
  }

  /** This is where you add some context
   *
   * @param string $context context['this'] Being the Twig's {{ this }}.
   */
  public function add_to_context( $context ) {
    ob_start();
    dynamic_sidebar( 'header_top' );
    $context['widget_header_top'] = ob_get_contents();
    ob_clean();
    ob_start();
    dynamic_sidebar( 'footer_top' );
    $context['widget_footer_top'] = ob_get_contents();
    ob_clean();
    ob_start();
    dynamic_sidebar( 'fixed' );
    $context['widget_fixed'] = ob_get_contents();
    ob_clean();
    $context['menu']              = new Timber\Menu( 'header_menu' );
    $context['footer_menu']       = new Timber\Menu( 'footer_menu' );
    $context['footer_short_menu'] = new Timber\Menu( 'footer_short_menu' );
    $context['language_code']     = ICL_LANGUAGE_CODE;

    return $context;
  }

  public function theme_supports() {
    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
    add_theme_support( 'title-tag' );

    /*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
    add_theme_support( 'post-thumbnails' );

    /*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
    add_theme_support(
      'html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
      )
    );

    /*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
    add_theme_support(
      'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
      )
    );

    add_theme_support( 'menus' );

    register_nav_menus( [
      'header_menu'       => __( 'Header menu', 'primary' ),
      'footer_menu'       => __( 'Footer menu', 'primary' ),
      'footer_short_menu' => __( 'Footer short menu', 'primary' ),
    ] );
  }

  public function translateString( $string, $name ) {
    return apply_filters( 'wpml_translate_single_string', $string, 'primary', $name );
  }

  public function localizeLinkById( $id ) {
    $link = get_permalink( $id );

    return apply_filters( 'wpml_permalink', $link );
  }

  public function localizeCategoryLinkById( $id ) {
    $link = get_category_link( $id );

    return apply_filters( 'wpml_permalink', $link );
  }

  public function localizeReviewForm( $content ) {
    $content = preg_replace('/own review<\/div><\/p>/', 'own review<\/div>', $content);

    $c = HtmlPageCrawler::create($content);
    $form_outer = $c->filter('.wpcr3_in_content');

    if (!$form_outer) return $content;

    $name = $form_outer->filter('input[name="wpcr3_fname"]');

    if (!$name) return $content;

    $name = $name->makeClone();
    $name->attr('placeholder', apply_filters( 'wpml_translate_single_string', 'Name', 'primary', 'Testimonial form - Name' ));

    $email = $form_outer->filter('input[name="wpcr3_femail"]')->makeClone();
    $email->attr('placeholder', apply_filters( 'wpml_translate_single_string', 'Email', 'primary', 'Testimonial form - Email' ));

    $rating = $form_outer->filter('input[name="wpcr3_ftext"]')->makeClone();
    $rating->setStyle('display', 'none');
    $rating->setAttribute('value', '5');

    $review = $form_outer->filter('textarea[name="wpcr3_ftext"]')->makeClone();
    $review->setAttribute('placeholder', apply_filters( 'wpml_translate_single_string', 'Review', 'primary', 'Testimonial form - Review' ));

    $check = $form_outer->filter('td.wpcr3_check_confirm > *')->makeClone();
    $check_wrap = HtmlPageCrawler::create('<div class="wpcr3_check_confirm"></div>');
    $check_wrap->append($check);
    $check_wrap_label = $check_wrap->filter('label');
    $input = $check_wrap_label->filter('input')->makeClone();
    $check_wrap_label->html('');
    $check_wrap_label->append($input);
    $check_wrap_label->append('&nbsp;'.apply_filters( 'wpml_translate_single_string', 'Check this box to confirm you are human.', 'primary', 'Testimonial form - Human checkbox' ));

    $submit = $form_outer->filter('.wpcr3_submit_btn')->makeClone();
    $submit->text(apply_filters( 'wpml_translate_single_string', 'Send', 'primary', 'Testimonial form - Send' ));

    $place = $form_outer->filter('.wpcr3_div_2');
    $place->html('');
    $place->append('<h2 class="wpcr3_leave_text">' . $this->translateString('Submit your Testimonials', 'Testimonials form - title') . '</h2>');
    $place->append($name);
    $place->append($email);
    $place->append($rating);
    $place->append($review);
    $place->append($check_wrap);
    $place->append($submit);

    return $c->saveHTML();
  }

  public function convertYotubeLinkToIframe($content) {
    preg_match_all('/(https:\/\/www\.youtube\.com\/.*?)[<|\t|\r|\n| ]/', $content, $out);

    if (count($out[1]) === 0) {
      return $content;
    }

    foreach ($out[1] as $match) {
      $embed_code = wp_oembed_get(trim($match), array('width' => 288));
      $content = str_replace($match, $embed_code, $content);
    }

    return $content;
  }

  /** This is where you can add your own functions to twig.
   *
   * @param string $twig get extension.
   */
  public function add_to_twig( $twig ) {
    $twig->addExtension( new Twig_Extension_StringLoader() );
    $twig->addFilter( new Twig_SimpleFilter( 'localizeReviewForm', array( $this, 'localizeReviewForm' ) ) );
    $twig->addFilter( new Twig_SimpleFilter( 'localizeLinkById', array( $this, 'localizeLinkById' ) ) );
    $twig->addFilter( new Twig_SimpleFilter( 'localizeCategoryLinkById', array( $this, 'localizeCategoryLinkById' ) ) );
    $twig->addFilter( new Twig_SimpleFilter( 'translateString', array( $this, 'translateString' ) ) );
    $twig->addFilter( new Twig_SimpleFilter( 'localizeLinkById', array( $this, 'localizeLinkById' ) ) );
    $twig->addFilter( new Twig_SimpleFilter( 'convertYotubeLinkToIframe', array( $this, 'convertYotubeLinkToIframe' ) ) );

    return $twig;
  }
}

new StarterSite();

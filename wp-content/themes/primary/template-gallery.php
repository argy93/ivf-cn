<?php
/* Template Name: Gallery
 * Template Post Type: post, page, product
 */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;

Timber::render( array( 'template-gallery.twig' ), $context );

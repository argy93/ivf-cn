<?php
$icons = array();
preg_match_all('|\[(.*?)\]|', block_field( 'icons', false ), $icons);

$context = array();
$context['data'] = [
  'percent' => block_field( 'percent', false ),
  'icons' => count($icons) ? $icons[1] : array(),
  'alignment' => block_field( 'alignment', false )
];

Timber::render( array( 'templates/partial/graph.twig' ), $context );
?>

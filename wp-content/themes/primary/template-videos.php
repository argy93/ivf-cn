<?php
/* Template Name: Videos
 * Template Post Type: post, page, product
 */


$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;

Timber::render( array( 'template-videos.twig' ), $context );
